# Collaboration

## Setup

### What you need for development

- [Git](https://git-scm.com/downloads)
- [Docker CE](https://docs.docker.com/install/)
- [VSCode](https://code.visualstudio.com/)
- [Remote Development Extension for VSCode](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack)

    VSCode Command (Open commandconsole within VSCode [CTRL+Shift+P])

    ```vscode
    ext install ms-vscode-remote.vscode-remote-extensionpack
    ```

    (No need to install other Extensions manualy. The Devcontainersetup will take care of them inside your local container)

### Get the environment running

- clone the main repo [https://gitlab.com/ros2-cuisine/cuisine](https://gitlab.com/ros2-cuisine/cuisine) recursivly into your desired place

    ```bash
    git clone https://gitlab.com/ros2-cuisine/cuisine/ --recursive
    ```

- Start Visual Studio Code
- set your git e-mail and name within the cuisine Container - Edit ```.devcontainer/template.Dockerfile``` but don't save the changes in this file -> rename it to ```.devcontainer/Dockerfile```

- Click on the Remote Dev Icon (left bottom corner); or open your VSCode Commandline (STRG+Shift+P) and type ```Remote-Containers:```
- Select ```Open Folder in Container```
- Select the repositoryfolder you just cloned
- Wait for docker to finish
- Don't forget to populate your dev-branch within specific repositories with the name pattern dev-{USERNAME}
- Make some changes
- Test your changes
- Contact the dev team with an issue to get write access for uploading your branch; or push your changes into your own remote origin and open a pull request
- Wait for the CI magic to build, test and merge your changes. (See [below](#dockerimage-lifecycle) when to expect changes to happen)

### Cheatsheet for development

#### Build your changes

```sh
./scripts.sh -b
```

#### Test your changes

```sh
./scripts.sh -t
```

#### Push your changes

```sh
./scripts.sh -p
```

#### Open a merge request

```sh
./scripts.sh -m
```

## Overview

```mermaid
graph TD

   subgraph Frontend
      Client[Client]
      FN[Frontend]
      LB[Load Balancer]
      Client --> LB
      LB --> Client
      LB --> FN
      FN --> LB
      end

    subgraph Backend
        Cnode[Configurator]
        Master[Cuisine Master Node]
        FN --> Cnode
        Cnode --> Master
        Master --> Intakenode
        Master --> Storagenode
        Master --> Disposalnode
        Master --> Processingnode
        end

    subgraph Tests and Simulation
        all[temporary Master Node]
        Gnode[Gazebonode]
        SN[Simulationnode]
        Master --> SN
        SN --> Master
        SN --> all
        Gnode --> SN
        all --> Gnode
        end

    subgraph Intakefunctions
        Intakenode --> Ifunctions[Functions]
        end

    subgraph Processingfunctions
        Processingnode --> Pfunctions[Functions]
        end

    subgraph Disposalfunctions
        Disposalnode --> Dfunctions[Functions]
        end

    subgraph Storagefunctions
        Storagenode --> Sfunctions[Functions]
        end
```

## Dockerimages Lifecyle

```mermaid
stateDiagram
    local --> testing
    development --> local
    development --> testing
    testing --> local
    testing --> development
    development --> nightly
    nightly --> latest
    latest --> lts
```

### LTS builds

release shedule: Will be released after dependencie major updates are tested properly + security patches if needed

First Release can hopefully be done with the next ubuntu/ros2 lts version ~ may 2022

### Latest builds

release shedule: ~ every 4 weeks or when needed

### Stable builds

release shedule: ~ twice a year + security patches if needed

### Nightly builds

Will be activated for the first tester

release shedule: weekly with the newest features as development seems to be working

### Testing builds

Created and pushed as needed and/or requested for collaborative bugfinding

## LICENSE

<a
    rel="license"
    href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
    <img
        alt="Creative Commons License"
        style="border-width:0"
        src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png"
    />
</a>
This work is licensed under a
<a  
    rel="license"
    href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
    Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License
</a>.
Permissions for companies and individuals beyond the scope of this license can be obtained and are shown at [Cuisine Buisness](https://buisness.cuisine.ichbestimmtnicht.de/).
