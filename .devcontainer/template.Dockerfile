# setup environment variables (ARG for settings can be changed at buildtime with --build-arg <varname>=<value>
ARG ROS_DISTRO=eloquent
ARG BUILD_VERSION=0.0.0

FROM ros2cuisine/vsc-master

ENV DEBIAN_FRONTEND noninteractive
ENV ROS_DISTRO eloquent
# Place your tests here

USER root

RUN sudo apt-get update \
    && apt-get install -y -q \
        ruby-dev \
    && sudo gem install github-markdown \
    && sudo gem install gollum \
    && sudo rm -rf /var/lib/apt/lists/* \
    && sudo rm -f /etc/timezone \
    && echo 'Europe/Berlin' > /etc/timezone \
    && sudo ln -s -f /usr/share/zoneinfo/Etc/UTC /etc/localtime

USER cuisine

# RUN mkdir -p ~/.ssh

# Authorize SSH Host
RUN mkdir -p ~/.ssh && \
    chmod 0700 ~/.ssh && \
    ssh-keyscan gitlab.com > ~/.ssh/known_hosts

ADD id_rsa /home/cuisine/.ssh/gitlab_id_rsa

# Add the keys and set permissions
RUN sudo chmod 600 ~/.ssh/gitlab_id_rsa


ENV GIT_EMAIL=""
ENV GIT_NAME=""
ENV GIT_KEY=""

RUN git config --global user.email "${GIT_EMAIL}" \
    && git config --global user.name "${GIT_NAME}" \
    && git config --global user.signinkey "${GIT_KEY}" \
    && mkdir -p /home/cuisine/.ssh \
    && cd /home/cuisine/.ssh \
    && echo "Host gitlab.com" > config \
    && echo "  Preferredauthentications publickey" >> config \
    && echo "  IdentityFile /home/cuisine/.ssh/gitlab_id_rsa" >> config \
    && sudo chown -R cuisine:cuisine /home/cuisine/.ssh
#    && pip3 install -U \
#        name
#    && cd foldername
#    && git clone --recursive https://

RUN ssh -oStrictHostKeyChecking=accept-new -T git@gitlab.com

RUN sudo apt-get update \
    && sudo apt-get install -qqy \
        x11-apps \
        bash-completion \
    && sudo rm -rf /var/lib/apt/lists/*

ENV DISPLAY :0

CMD ["xeyes"]
# Instructions to a child image build
# ONBUILD RUN sudo apt update
