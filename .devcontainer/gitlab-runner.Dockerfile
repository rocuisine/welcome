FROM gitlab/gitlab-runner:alpine

ARG PROJECT_REGISTRATION_TOKEN
ARG TAG_LIST="docker, gitlab, cuisine"
ARG NUMBER=1

RUN gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "${PROJECT_REGISTRATION_TOKEN}" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "cuisine-runner-$NUMBER" \
  --tag-list "${TAG_LIST}" \
  --run-untagged="true" \
  --locked="false" \
  --access-level="not_protected"
