
# Cuisine

NOTE: This project is under heavy development and not suitable for production yet. If you like to help: Contact me and we will find a way to collaborate.

## Motivation

Cooking is good fun as well as tinkering so why not combine them?

I am an apprentice Software developer and do this as a hobby as well as a learning method.

### Goals

- No featurerestrictions for private users
- Enabling the average enduser to combine and add robotic/automation tasks primarly to process food
- Crash only (needed for saftey in case of an electricity cut anyway)
- Functional Programming / Serverless (Keeping program parts small and interchangable -> only load/activate whats needed)
- Cloud ready (Docker/Kubernetes)
- Creating something I like to keep
- Having fun and learning

### Inspirations

- [farm.bot](https://farm.bot/) (Bridge between is planned)
- [Homeassistant](https://hass.io/) (Bridge between is planned)
- [ESP Home YAML](https://esphome.io/) (Bridge between is planned)
- [IFTTT](https://ifttt.com/) (Bridge between is planned)

## Buisness (NOT PRODUCTION READY!)

Head to [this](http://buisness.cuisine.ichbestimmtnicht.de/) (Redirection to BUISNESS.md at the moment) Website and have a look around to get started.

## PREVIEW/PLANNED Start as new user - go to the Development Section for now

- Note: In case you wan't the packages for your existing ROS installation head over to [this section](https://gitlab.com/ros2cuisine/cuisine)
- Open your terminal and use a script as guide through the possible configuration options. Nothing will be changed by the script.
  - Linux

    ```sh
    curl -sSL https://gitlab.com/ros2cuisine/cuisine/blob/master/scripts/new_install.sh | sh
    ```

  - Windows HELP NEEDED

      ```powershell
      CALL https://gitlab.com/ros2cuisine/cuisine/blob/master/scripts/new_install.ps
      ```

  - Mac HELP NEEDED

    ```sh
    curl -sSL https://gitlab.com/ros2cuisine/cuisine/blob/master/scripts/new_install.sh | sh
    ```

- Now a complete easy to read script is created that you can review before starting it with the command shown by the previous script.
- The should print the webadress like ```https://localhost/Admin``` for your browser to open the admininterface.
- Add your [hardware.](https://gitlab.com/ros2cuisine/hardware/hardware.wiki.git)
- In case you are desperate you can try to add some [additional service tasks](https://gitlab.com/ros2cuisine/cuisine.wiki/Addtional_Tasks.md) along with the further setup.
- Do a full [simulation.](https://gitlab.com/ros2cuisine/simulation/simulation.wiki.git)
- [Add a meal.](https://gitlab.com/ros2cuisine/cuisine.wiki/Add_a_meal.md)
- [Simulate a meal](https://gitlab.com/ros2cuisine/simulation/simulation.wiki/Simulate_a_meal.md) with your hardware.
- Now you are ready to let the magic happen for the first time. \
    IMPORTANG: Do not let your setup unattended. You are responsible for potential damage! \
    It is recommended to get at least a proper insurcance policy in advance.

### Issues

If you are unsure where to post your Issue or to fiend help have a look at [this wiki page](https://gitlab.com/ros2cuisine/cuisine/wiki/Issues) and/or ask for help.

## Development

Have a look at [CONTRIBUTING.md](https://gitlab.com/ros2cuisine/cuisine/tree/master/CONTRIBUTING.md)

## LICENSE

<a
    rel="license"
    href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
    <img
        alt="Creative Commons License"
        style="border-width:0"
        src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png"
    />
</a>
This work is licensed under a
<a  
    rel="license"
    href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
    Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License
</a>.
Permissions for companies and individuals beyond the scope of this license can be obtained and are shown at [Cuisine Buisness](https://buisness.cuisine.ichbestimmtnicht.de/).
